import java.util.ArrayList;
import java.util.List;

/**
 * Created by tzhou on 4/3/17.
 */
public class SafeArray<T> {
    List<T> _arr = new ArrayList<T>();



    public synchronized void add(T e) {
        _arr.add(e);
    }

    public synchronized T get(int i) {
        return _arr.get(i);
    }

    public int size() {
        return _arr.size();
    }
}
