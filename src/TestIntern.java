/**
 * Created by tzhou on 3/24/17.
 */
public class TestIntern {

    /* create a scenario that trys to access the intern table after a migration */
    public static void main(String[] args) {
        String s1 = "Test";
        String s2 = "Test";
        String s3 = new String("Test");
        final String s4 = s3.intern();
        System.out.println(s1 == s2);
        System.out.println(s2 == s3);
        System.out.println(s3 == s4);
        System.out.println(s1 == s3);
        System.out.println(s1 == s4);
    }
}