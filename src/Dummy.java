import java.util.Random;

/**
 * Created by tzhou on 3/19/17.
 */
public class Dummy {
    int _a = 1;
    String _str;

    public Dummy() {
        Random generator = new Random();
        _a = generator.nextInt();
        _str = Integer.toString(_a);
    }

    public int a() {
        return _a;
    }

    public String str() {
        return _str;
    }
}
