import java.util.ArrayList;
//import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;

/**
 * Created by tzhou on 3/19/17.
 */
public class DummyArray {
    List<Dummy> _arr;
    int _push_count = 10000;
    int _acc_count = _push_count/100;
    int _loop_times = 100;
    int _sum = 0;

    public void set_push_count(int _push_count) {
        this._push_count = _push_count;
    }

    public void set_acc_count(int _acc_count) {
        this._acc_count = _acc_count;
    }

    public void set_loop_times(int _loop_times) {
        this._loop_times = _loop_times;
    }

    public int get_push_count() {
        return _push_count;
    }

    public int get_acc_count() {
        return _acc_count;
    }

    public int get_loop_times() {
        return _loop_times;
    }

    public void loop() {
        for (int i = 0; i < _loop_times; i++) {
            run();
        }
    }

    public void run() {
        String ss = "";
        _arr = new ArrayList<Dummy>();
        for (int i = 0; i < _push_count; i++) {
            Dummy d = new Dummy();
            _arr.add(d);
        }

        Random generator = new Random();
        for (int i = 0; i < _acc_count; i++) {
            int j = generator.nextInt(_push_count-1);
            _sum += _arr.get(j).a();
            ss += _arr.get(j).str();
        }
        System.out.println("one iteration");

    }

    public static void main(String[] args) {
        DummyArray dm = new DummyArray();

        if (args.length == 1) {
            int lt = Integer.parseInt(args[0]);
            dm.set_loop_times(lt);
        }
        else if (args.length == 3) {
            int pt = Integer.parseInt(args[0]);
            int at = Integer.parseInt(args[1]);
            int lt = Integer.parseInt(args[2]);
            dm.set_push_count(pt);
            dm.set_acc_count(at);
            dm.set_loop_times(lt);
        }

        System.out.printf("args num: %d", args.length);
        System.out.printf("push: %d, access: %d, loop: %d\n", dm.get_push_count(), dm.get_acc_count(), dm.get_loop_times());
        dm.loop();
        
    }
}
